import React, { useEffect, useState } from 'react';


function ConferenceForm(){
    const [location, setLocation] = useState([])
    const [name, setName] = useState("")
    const [starts, setStart] = useState("")
    const [ends, setEnd] = useState("")
    const [description, setDescription] = useState("")
    const [presentations, setMaxPresentations] = useState("")
    const [attendees, setMaxAttendees] = useState("")
    const [locations, setLocations] = useState("")


    const handleName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStarts = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEnds = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocations = (event) => {
        const value = event.target.value;
        setLocations(value);
    }

    const fetchData = async () => {
        const url = `http://localhost:8000/api/locations/`;
        const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setLocation(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends= ends;
        data.description= description;
        data.max_presentations= presentations;
        data.max_attendees= attendees;
        data.location= locations;
        console.log(data)

        const locationUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                const newLocation = await response.json();
                console.log(newLocation)

                setName('');
                setStart('');
                setEnd('');
                setDescription('');
                setMaxPresentations('');
                setMaxAttendees('');
                setLocations('');

            }

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleName} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStarts} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEnds} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="Desciption" className="form-label">Description</label>
                <textarea onChange={handleDescription} value={description} className="form-control" name="description" id="desciption" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentations} value={presentations} placeholder="MMaximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="maximum_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendees} value={attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="maximum_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocations} value={locations} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {location.map(location => {
                        return (
                        <option key={location.id} value={location.id}>
                        {location.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

};
export default ConferenceForm;
